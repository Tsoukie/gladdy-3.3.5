# Gladdy for 3.3.5a client (WotLK, TBC)
**`The most powerful arena addon for WoW, backported for 3.3.5a client private servers.`**

<img src="https://i.imgur.com/zWXfMNU.png" width="50%">

---

### ⚠ Note: This version is not related or affiliated with the official retail/classic addon!
### 📥 [Installation](#-installation-1)
### 📋 [Report Issue](https://gitlab.com/Tsoukie/gladdy-3.3.5/-/issues)
### 💬 [FAQ](#-faq-1)
### ❤️ [Support & Credit](#%EF%B8%8F-support-credit-1)

---
### Modules:
- **Announcement:** _Textual alert for: drink, trinket usage, spec detection_
- **ArenaCountDown:** _Numrical countdown until match begins_
- **Auras:** _Important (de)buffs, as well as interrupts displayed on class icon_
- **BuffsDebuffs:** _Arena unit (de)buffs - can be filtered_
- **CastBar:** _Arena unit castbar - can be disabled_
- **ClassIcon:** _Arena unit class or spec icon - once detected_
- **Clicks:** _Bind spells or macros to click actions_
- **CombatIndicator:** _Display a sword icon if arena unit is affected by combat_
- **Cooldown:** _Display and track important cooldowns_
- **Diminishing:** _Display and track crowd control DRs_
- **ExportImport:** _Share your profile with friends in the form of a string, which can be imported_
- **Highlight:** _Highlights focus and target units_
- **Pets:** _Display arena unit pets_
- **Racial:** _Display arena unit racial cooldowns_
- **Range Check:** _Checks the range of an arena unit by a configurable spell_
- **Shadowsight Timer:** _Display a (movable) Shadow Eyes spawn timer_
- **TotemPlates:** _Display icons above totems instead of normal nameplates_
- **Trinket:** _Display arena unit trinket cooldowns_
- **VersionCheck:** _Make sure you are using the most up-to-date gladdy version with your teammate_
- **XiconProfiles:** _Pre-defined profiles to help you get started on your way to dominate the arena_

### Commands:
- **`/gladdy ui`** _Display config window_
- **`/gladdy test`** _Test Mode: 3v3_
- **`/gladdy test1`** to **`/gladdy test5`** - _Test Mode: Display up to given number (1-5)_
- **`/gladdy hide`** _Hides frame(s)_
- **`/gladdy reset`** _Resets current profile to default settings_

<!-- blank line -->
<br>
<!-- blank line -->

# 📥 Installation

1. Download Latest Release `[.zip, .gz, ...]`:
	- <a href="https://gitlab.com/Tsoukie/gladdy-3.3.5/-/releases/permalink/latest" target="_blank">`📥 Gladdy-3.3.5`</a>
	- <a href="https://gitlab.com/Tsoukie/classicapi/-/releases/permalink/latest" target="_blank">`📥 ClassicAPI`</a> **⚠Required**
2. Extract **both** the downloaded compressed files _(eg. Right-Click -> Extract-All)_.
3. Navigate within each extracted folder(s) looking for the following: `!!!ClassicAPI` or `Gladdy`.
4. Move folder(s) named `!!!ClassicAPI` or `Gladdy` to your `Interface\AddOns\` folder.
5. Re-launch game.

<!-- blank line -->
<br>
<!-- blank line -->


# 💬 FAQ

> Why does totem icons not work with X nameplate addon?

I haven't had time to add special support for each and every nameplate addon. Make a report so it can be looked at.

> Why is the totem pulse module missing?

Due to older (3.3.5a) client limitations of not being able to accurately determine who the totem nameplate belongs to.

> I found a bug!

Please 📋 [report the issue](https://gitlab.com/Tsoukie/gladdy-3.3.5/-/issues) with as much detail as possible.

<!-- blank line -->
<br>
<!-- blank line -->


# ❤️ Support & Credit
 
If you wish to show some support you can do so [here](https://streamlabs.com/tsoukielol/tip). Tips are completely voluntary and aren't required to download my projects, however, they are _very_ much appreciated. They allow me to devote more time to creating things I truly enjoy. 💜

<!-- blank line -->
<br>
<!-- blank line -->
  
_This_ version is modified and maintained by [Tsoukie](https://gitlab.com/Tsoukie/) and is **not** related or affiliated with any other version.

### Original AddOn
**Author:** [XiconQoo](https://github.com/XiconQoo/) (not related/affiliated with this version)

- **Contributors**
    - [XyzKangUI](https://github.com/XyzKangUI), [ManneN1](https://github.com/ManneN1), [AlexFolland](https://github.com/AlexFolland), [dfherr](https://github.com/dfherr), [miraage](https://github.com/miraage), [veiz](https://github.com/veiz)
    - **Special Thanks**: Schaka, Macumba, RMO, Ur0b0r0s aka DrainTheLock, Klimp, the whole TBC addons 2.4.3 discord, Hydra, Xyz